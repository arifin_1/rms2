package com.mitrais.rms.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitrais.rms.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}
