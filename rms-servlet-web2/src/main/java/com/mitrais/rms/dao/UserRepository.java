package com.mitrais.rms.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitrais.rms.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	
	public Optional<User> findByUserNameAndEnabled(String username, short enabled);
	
	public Optional<User> findByUserName(String username);
	
	public void deleteByUserName(String username);
	
}
