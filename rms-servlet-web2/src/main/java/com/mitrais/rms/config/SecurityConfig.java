package com.mitrais.rms.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private MyAppUserDetailsService myAppUserDetailsService;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/err/**").permitAll()
			.antMatchers("/secure/home", "/secure/user/changepass").hasRole("USER")
			.antMatchers("/secure/user/**", "/secure/role/**").hasRole("ADMIN")
			.antMatchers("/secure/**").authenticated()
		.and()
			.formLogin()
	        .loginPage("/login")
	        .loginProcessingUrl("/app-login")
	        .usernameParameter("username")
	        .passwordParameter("password")
	        .defaultSuccessUrl("/secure/home")
		.and()
			.logout() //logout configuration
			.logoutUrl("/app-logout")
			.logoutSuccessUrl("/login")
		.and()
			.exceptionHandling() //exception handling configuration
			.accessDeniedPage("/err/403");
	}
	
    @Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myAppUserDetailsService).passwordEncoder(passwordEncoder());
	}
    
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }
    
}