package com.mitrais.rms.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mitrais.rms.dao.UserRepository;
import com.mitrais.rms.model.Role;

@Service
public class MyAppUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
	@Override
	public UserDetails loadUserByUsername(String userName)
			throws UsernameNotFoundException {
		com.mitrais.rms.model.User activeUser = userRepository.findByUserNameAndEnabled(userName, (short) 1)
				.orElseThrow(() -> new RuntimeException("Username is not found."));
		
		logger.debug(activeUser.getFullName());
		logger.debug(Arrays.toString(activeUser.getRoles().toArray()));
		
		Set<GrantedAuthority> authorities = new HashSet<>();
		for (Role role : activeUser.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
		}
		
		UserDetails userDetails = (UserDetails)new User(activeUser.getUserName(),
				activeUser.getPassword(), authorities);
		return userDetails;
	}
	
}

