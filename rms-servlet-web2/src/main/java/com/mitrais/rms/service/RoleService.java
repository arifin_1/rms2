package com.mitrais.rms.service;

import java.util.List;

import com.mitrais.rms.model.Role;

public interface RoleService {
	
	public List<Role> findAll();
	
	public Role findById(int theId) throws Exception;
	
	public void save(Role theRole);
	
	public void deleteById(int theId);
	
}
