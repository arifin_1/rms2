package com.mitrais.rms.service;

import java.util.List;

import org.springframework.security.access.annotation.Secured;

import com.mitrais.rms.model.User;

public interface UserService {
	
	public List<User> findAll();
	
	public User findById(int theId) throws Exception;
	
	public User findByUsername(String userName) throws Exception;
	
	public void save(User formUser);

	@Secured ({"ROLE_ADMIN"})
	public void deleteById(int theId);
	
	public void deleteByUsername(String userName);
	
	public boolean isPasswordMatch(String rawPass, String encodedPass);
	
	public void changePassword(User theUser, String newPassword);
	
}
