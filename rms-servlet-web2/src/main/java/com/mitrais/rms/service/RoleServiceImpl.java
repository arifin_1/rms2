package com.mitrais.rms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitrais.rms.dao.RoleRepository;
import com.mitrais.rms.model.Role;

@Service
public class RoleServiceImpl implements RoleService {

	private final RoleRepository roleRepository;
	
	@Autowired
	public RoleServiceImpl(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}
	
	@Override
	public List<Role> findAll() {
		return roleRepository.findAll();
	}

	@Override
	public Role findById(int theId) throws Exception {
		Role theRole = roleRepository.findById(theId)
				.orElseThrow(() -> new Exception("Role with id " + theId + " is not found."));
		
		return theRole;
	}

	@Override
	public void save(Role theRole) {
		roleRepository.save(theRole);
	}

	@Override
	public void deleteById(int theId) {
		roleRepository.deleteById(theId);
	}

}
