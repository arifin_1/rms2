package com.mitrais.rms.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.mitrais.rms.dao.UserRepository;
import com.mitrais.rms.model.User;

@Service
public class UserServiceImpl implements UserService {
	
	private final UserRepository userRepository;
	
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public User findById(int theId) throws Exception {
		User theUser = userRepository.findById(theId)
				.orElseThrow(() -> new Exception("User with id " + theId + " is not found."));
		
		return theUser;
	}

	@Override
	public User findByUsername(String userName) throws Exception {
		User theUser = userRepository.findByUserName(userName)
				.orElseThrow(() -> new Exception("User with username " + userName + " is not found."));
		
		return theUser;
	}

	@Override
	@Transactional
	public void save(User formUser) {
		User currentUser = new User();
		if (formUser.getId() != 0) {
			currentUser = userRepository.findById(formUser.getId()).get();
		} else {
			currentUser.setPassword(bCryptPasswordEncoder.encode("123456"));
			currentUser.setEnabled((short) 1);
		}
		
		currentUser.setUserName(formUser.getUserName());
		currentUser.setRoles(formUser.getRoles());
		currentUser.setFullName(formUser.getFullName());
		currentUser.setCountry(formUser.getCountry());
		
		System.out.println(currentUser.toString());
		
		userRepository.save(currentUser);
	}

	@Override
	public void deleteById(int theId) {
		userRepository.deleteById(theId);
	}

	@Override
	@Transactional
	public void deleteByUsername(String userName) {
		userRepository.deleteByUserName(userName);
	}

	@Override
	public boolean isPasswordMatch(String rawPass, String encodedPass) {
		return bCryptPasswordEncoder.matches(rawPass, encodedPass);
	}

	@Override
	public void changePassword(User theUser, String newPassword) {
		theUser.setPassword(bCryptPasswordEncoder.encode(newPassword));
		userRepository.save(theUser);
	}
	
}
