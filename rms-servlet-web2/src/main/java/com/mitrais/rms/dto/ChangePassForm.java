package com.mitrais.rms.dto;

public class ChangePassForm {
	
	private String userName;
	private String oldPassword;
	private String newPassword1;
	private String newPassword2;
	
	public ChangePassForm() { }

	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getOldPassword() {
		return oldPassword;
	}
	
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	public String getNewPassword1() {
		return newPassword1;
	}
	
	public void setNewPassword1(String newPassword1) {
		this.newPassword1 = newPassword1;
	}
	
	public String getNewPassword2() {
		return newPassword2;
	}
	
	public void setNewPassword2(String newPassword2) {
		this.newPassword2 = newPassword2;
	}

	@Override
	public String toString() {
		return "ChangePass [userName=" + userName + ", oldPassword=" + oldPassword
				+ ", newPassword1=" + newPassword1 + ", newPassword2=" + newPassword2 + "]";
	}
	
}
