package com.mitrais.rms.controller;

import java.io.UnsupportedEncodingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mitrais.rms.model.Role;
import com.mitrais.rms.service.RoleService;

@Controller
@RequestMapping("secure/role")
public class RoleController {
	
	private final RoleService roleService;
	
	@Autowired
	public RoleController(RoleService roleService) {
		this.roleService = roleService;
	}
	
	@GetMapping("")
	public ModelAndView list() {
		ModelAndView mav = new ModelAndView();
		mav.addObject("roles", roleService.findAll());
		mav.setViewName("role/list");
		return mav;
	}
	
	@GetMapping("form")
	public ModelAndView form(@RequestParam(required = false) Integer id) throws UnsupportedEncodingException {
		Role theRole = new Role();
		if (id != null) {
			try {
				theRole = roleService.findById(id);
			} catch (Exception e) {
				return new ModelAndView("redirect:/err/400/Role with id " + id + " is not found.");
			}
		}

		ModelAndView mav = new ModelAndView();
		mav.addObject("role", theRole);
		mav.setViewName("role/form");
		return mav;
	}
	
	@PostMapping("")
	public String save(@ModelAttribute Role theRole) {
		roleService.save(theRole);
		return "redirect:/secure/role";
	}
	
	@GetMapping("delete")
	public String delete(@RequestParam int id) {
		Role theRole;
		try {
			theRole = roleService.findById(id);
		} catch (Exception e) {
			return "redirect:/err/400/Role with id " + id + " is not found.";
		}
		
		if (theRole.getUsers().size() == 0) {
			roleService.deleteById(id);
		} else {
			return "redirect:/secure/role?error=Role has been bound with user(s).";
		}
		
		return "redirect:/secure/role?success=Role has been deleted.";
	}
	
}
