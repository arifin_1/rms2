package com.mitrais.rms.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mitrais.rms.dto.ChangePassForm;
import com.mitrais.rms.model.User;
import com.mitrais.rms.service.RoleService;
import com.mitrais.rms.service.UserService;

@Controller
public class UserController implements ErrorController {
	
	private final UserService userService;
	
	private final RoleService roleService;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
	@SuppressWarnings("serial")
	private final Map<Integer, Map<String, String>> errorCodes = new HashMap<Integer, Map<String, String>>() {
		private static final long serialVersionUID = 1L;
		{
			put(400, new HashMap<String, String>() {{
				put("head", "Bad Request");
				put("defMessage", "The request parameter(s) you have given is not found.");
			}});
			put(403, new HashMap<String, String>() {{
				put("head", "Access Denied");
				put("defMessage", "You are not authorized for the requested data.");
			}});
			put(404, new HashMap<String, String>() {{
				put("head", "Not Found");
				put("defMessage", "The page you are looking for is not found.");
			}});
		}
	};
	
	@Autowired
	public UserController(UserService userService, RoleService roleService) {
		this.userService = userService;
		this.roleService = roleService;
	}
	
	@GetMapping("login")
	public ModelAndView login(Principal principal) {
	    ModelAndView mav = new ModelAndView();
	    mav.setViewName("login");
	    
	    if (principal != null) {
	    	return new ModelAndView("redirect:/secure/home");
	    } else {
	    	return mav;
	    }
    }
	
	@GetMapping("secure/home")
	public ModelAndView home() {
	    ModelAndView mav = new ModelAndView();
	    mav.setViewName("home");
	    return mav;
    }
	
	@GetMapping("secure/user")
	public ModelAndView list(Principal principal) {
		ModelAndView mav = new ModelAndView();
		List<User> users = userService.findAll();
		
		mav.addObject("username", principal.getName());
		mav.addObject("users", users);
		mav.setViewName("user/list");
		return mav;
	}
	
	@GetMapping("secure/user/form")
	public ModelAndView form(@RequestParam(required = false) String username) {
		User theUser = new User();
		if (username != null) {
			try {
				theUser = userService.findByUsername(username);
			} catch (Exception e) {
				return new ModelAndView("redirect:/err/400/The username is not found.");
			}
		}
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("user", theUser);
		mav.addObject("allRoles", roleService.findAll());
		mav.setViewName("user/form");
		return mav;
	}
	
	@PostMapping("secure/user")
	public String save(@ModelAttribute User formUser) throws UnsupportedEncodingException {		
		userService.save(formUser);
		return "redirect:/secure/user?success=" + URLEncoder.encode("User " + formUser.getUserName() + " has been saved.", "UTF-8");
	}

	@GetMapping("secure/user/delete")
	public String delete(@RequestParam String username, Principal principal) throws UnsupportedEncodingException {
		if (principal.getName().equals(username)) {
			return "redirect:/secure/user?error=" + URLEncoder.encode("You cannot delete your account.", "UTF-8");
		}
		
		userService.deleteByUsername(username);
		return "redirect:/secure/user?success=" + URLEncoder.encode("User " + username + " has been saved.", "UTF-8");
	}
	
	@GetMapping("secure/user/changepass")
	public ModelAndView changepass(Principal principal) {
		ChangePassForm changePass = new ChangePassForm();
		changePass.setUserName(principal.getName());
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("changePass", changePass);
		mav.setViewName("user/changepass");
		return mav;
	}
	
	@PostMapping("secure/user/changepass")
	public String changepasssave(ChangePassForm formPass) throws UnsupportedEncodingException {
		User theUser;
		try {
			theUser = userService.findByUsername(formPass.getUserName());
		} catch (Exception e) {
			return "redirect:/err/400/The username is not found.";
		}
		
		if (formPass.getOldPassword().equals("") || formPass.getNewPassword1().equals("") || formPass.getNewPassword2().equals("")) {
			return "redirect:/secure/user/changepass?error=" + URLEncoder.encode("Passwords must not be empty.", "UTF-8");
		} else if (!formPass.getNewPassword1().equals(formPass.getNewPassword2())) {
			return "redirect:/secure/user/changepass?error=" + URLEncoder.encode("New passwords do not match.", "UTF-8");
		} else if (!userService.isPasswordMatch(formPass.getOldPassword(), theUser.getPassword())) {
			return "redirect:/secure/user/changepass?error=" + URLEncoder.encode("Old password does not match with current password.", "UTF-8");
		} else {
			userService.changePassword(theUser, formPass.getNewPassword1());
		}
		
		return "redirect:/secure/user/changepass?success=" + URLEncoder.encode("Your password has been changed.", "UTF-8");
	}
	
	@GetMapping({"err", "err/{id}", "err/{id}/{message}"})
	public ModelAndView error(Principal principal, @PathVariable Optional<Integer> id, @PathVariable Optional<String> message) {
		int errorCode = id.orElse(403);
		String errorMessage = message.orElse(errorCodes.get(errorCode).get("defMessage"));
		logger.debug(errorCode + " - " + errorMessage);
		
	    ModelAndView mav = new ModelAndView();
	    mav.addObject("errorHead", errorCodes.get(errorCode).get("head"));
	    mav.addObject("errorMsg", errorMessage);
	    mav.setViewName("costum-error");
	    return mav;
    }
	
	@GetMapping("error")
	public String error() {
		return "redirect:/err/404";
	}

	@Override
	public String getErrorPath() {
		return "error";
	}
	
}